const StripesWebpackPlugin = require( "@folio/stripes-webpack/webpack/stripes-webpack-plugin" );
const StripesConfigPlugin = require('@folio/stripes-webpack/webpack/stripes-config-plugin');
const stripesSerialize = require('@folio/stripes-webpack/webpack/stripes-serialize');

const serialize = require('serialize-javascript');
 
console.log( "Loading local .stripesclirc.js");

class PluginWrapper {
  
  plugin = null
  
  constructor(originalPlugin) {
    this.plugin = originalPlugin
  }
  
  apply(compiler) {
    
  }
}

const beforeBuildFunction = (args) => {
  // This method is called with the args supplied to the handler
  // for the command. The return function of this function should be
  // another function that receives the webpack config and then returns
  // it again with any modifications complete.
  console.log ("serve.beforeBuild called in .stripesclirc.js");
  
  return (conf) => {
    console.log("Webpack override called from .stripesclirc.js");
    console.log("Current plugins: o%", conf.plugins);
    
    conf.plugins = conf.plugins.map(plugin => {
      if (plugin instanceof StripesWebpackPlugin) {
        const previousPlugin = plugin;
        // Wrap this plugin.
        const WrappedStripesWebpackPlugin = {
          
          replaceStripesConfigMod() {
            const modded = new StripesConfigPlugin(previousPlugin.stripesConfig);
            
            modded.afterPlugins = function(compiler) {
              // Data provided by other stripes plugins via hooks
              const pluginData = {
                branding: {},
                errorLogging: {},
                translations: {},
              };

              StripesConfigPlugin.getPluginHooks(compiler).beforeWrite.call(pluginData);

              // Create a virtual module for Webpack to include in the build
              const stripesVirtualModule = `
                const { okapi, config, modules } = ${serialize(this.mergedConfig, { space: 2 })};
                const branding = ${stripesSerialize.serializeWithRequire(pluginData.branding)};
                const errorLogging = ${stripesSerialize.serializeWithRequire(pluginData.errorLogging)};
                const translations = ${serialize(pluginData.translations, { space: 2 })};
                const metadata = ${stripesSerialize.serializeWithRequire(this.metadata)};
                const icons = ${stripesSerialize.serializeWithRequire(this.icons)};
                
                // Move defined properties...
                okapi._url = okapi.url;
                okapi._tenant = okapi.tenant;
                
                // Override with getters and setters.
                Object.defineProperties(okapi, {
                  tenant: {
                    set: function( tenant ) {
                      // Setter always replaces the local value.
                      this._tenant = tenant;
                    },
                    get: function() {
                      return (typeof this.tenantRuntime === 'function' ? this.tenantRuntime() : this._tenant);
                    }
                  },
                  url: {
                    set: function( url ) {
                      // Setter always replaces the local value.
                      this._url = url;
                    },
                    get: function() {
                      return (typeof this.urlRuntime === 'function' ? this.urlRuntime() : this._url);
                    }
                  }
                });
                
                export { okapi, config, modules, branding, errorLogging, translations, metadata, icons };
              `;

              console.log('writing replaced virtual module');
              this.virtualModule.writeModule('node_modules/stripes-config.js', stripesVirtualModule);
            }
            
            return modded;
          },
          
          apply(compiler) {
            console.log ("PRE PLUGIN");
            previousPlugin.apply(compiler);
            console.log("POST PLUGIN");
            this.replaceStripesConfigMod().apply(compiler);
          }
        };
        return WrappedStripesWebpackPlugin;
      }
      
      // Default 
      return plugin;
    });
    
    console.log("Altered plugins: o%", conf.plugins);
    return conf;
  }
}



module.exports = {
  plugins: {
    "serve": {
      "beforeBuild": beforeBuildFunction
    },
    "build": {
      "beforeBuild": beforeBuildFunction
    }
  }
}
