#!/usr/bin/env node

const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
//const updateNotifier = require('update-notifier');
//const isInstalledGlobally = require('is-installed-globally');
const packageJson = require('./package.json');
const AliasError = require('@folio/stripes-cli/lib/platform/alias-error');
const { yargsConfig, commandDirOptions } = require('@folio/stripes-cli/lib/cli/config');
const logger = require('@folio/stripes-cli/lib/cli/logger')();
const path = require('path');

process.title = 'stripes-cli-ext';
logger.log('stripes-cli-ext', packageJson.version);

// Update notifier runs async in a child process
//updateNotifier({
//  pkg: packageJson,
//  updateCheckInterval: 1000 * 60 * 60 * 24 * 7,
//}).notify({
//  isGlobal: isInstalledGlobally,
//  // TODO: Consider reverting to default message once update-notifier detects global yarn installs
//  message: 'Update available - Refer to README.md:\nhttps://github.com/folio-org/stripes-cli',
//});

const cliDir = path.dirname( require.resolve('@folio/stripes-cli') )

try {
  yargs(hideBin(process.argv))
  
    // Load originals
    .commandDir(path.resolve(cliDir, './lib/commands'), commandDirOptions) // NOSONAR
    
    // Load any locals, including redeclared overrides.
    .commandDir(path.resolve('./commands'), commandDirOptions) // NOSONAR
    .config(yargsConfig)
    .option('interactive', {
      describe: 'Enable interactive input (use --no-interactive to disable)',
      type: 'boolean',
      default: true,
      hidden: true,
    })
    .completion()
    .recommendCommands()
    .example('$0 <command> --help', 'View examples and options for a command')
    .demandCommand()
    .env('STRIPES')
    .scriptName('stripes')
    .help()
    .showHelpOnFail(false)
    .wrap(yargs().terminalWidth())
    .parse();
} catch (err) {
  if (err instanceof AliasError) {
    console.log(`Alias Error: ${err.message}`);
  } else {
    throw err;
  }
}
