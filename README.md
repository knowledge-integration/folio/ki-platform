
See

https://repository.folio.org/#browse/browse:npm-ci-all to browse the CI registry
https://hub.docker.com/r/folioci/mod-service-interaction/tags



Run locally with 
npm install

export STRIPES_TRANSPILE_TOKENS="@k-int @reshare";
yarn run stripes serve ./stripes.config.js 


WHEN MANUALLY UPDATING A CHANNEL - Don't forget to copy all of

resolved_specification.json
channel.json
enable.json
install.json



For tooling

    npm install --package-lock-only
