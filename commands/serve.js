const originalBuildObject = require('@folio/stripes-cli/lib/commands/serve');

function extendedHandler(argv, extensions) {
  
  const beforeBuild = extensions?.plugin?.beforeBuild;
  if (beforeBuild) {
    
    if (!argv.context) {
      argv.context = {};
    }
    
    if (!argv.context.plugin) {
      argv.context.plugin = {};
    }
    
    argv.context.plugin.beforeBuild = beforeBuild;
  }

  originalBuildObject.handler(argv);
}

// Re export the original with the exception of replacing the 'handler'
// with our extended version above.
const newObj = Object.assign({}, originalBuildObject, {
  handler: extendedHandler
});

module.exports = newObj;
