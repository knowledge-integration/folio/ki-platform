#!/bin/bash


curl -k \
     -H "Accept: application/json" \
     -H "Content-Type: application/json" \
     -X POST -d @./channel_specification.json \
     "https://apps2.sph.k-int.com/tlpapi/public/resolvePlatformSpecification?verify=check" | jq > ./resolved_specification.json


# "https://apps.libsdev.k-int.com/tlpapi/public/resolvePlatformSpecification?verify=check" | jq > ./resolved_specification.json

export MODULE_STATUS=`cat ./resolved_specification.json | jq -r ".status"`

if [ $MODULE_STATUS == valid ]
then
  echo module validates - can be published
  cat ./resolved_specification.json | jq ".activations[] | select(.id | startswith(\"mod-\"))" | jq -s > ./channel.json
  cat ./resolved_specification.json | jq ".activations[] | select(.id)" | jq -s > ./enable.json
  touch IS_VALID
else
  echo MODULE CONFIGURATION IS INVALID - SKIP
  if [ -f IS_VALID ]
  then
    rm IS_VALID
  fi
fi
