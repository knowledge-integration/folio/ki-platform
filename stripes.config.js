const getTenantData = () => {
  const conf = {
  	// _url:'https://okapi-dev.libsdev.k-int.com',
  	_url:'https://okapi-dev.libsdev.k-int.com',
    _tenant: 'test1',
    _tenant_cache: {},
    _tlp_api:'https://apps2.sph.k-int.com',
  	
	tenantRuntime: function() {
	  if (typeof window === 'undefined') {
        // this seems to happen in deep stripes requests on the users browser, so we're now setting this._tenant
        // early on in the process so we can return a cached value.
        console.log("window is undefined(tenant)....");
	  }
      else {
        var tn = this.extractTenant(window.location.host)?.tenantIdentifier;
        // console.log("call extract tenant(%s,%o) returning %s",window.location.host, this._tenant_cache, tn);
        this._tenant = tn;
      }
      return this._tenant;
	},
	
    urlRuntime: function() {
	  if (typeof window === 'undefined') {
         console.log("window is undefined(url)....");
	  }
      else {
    	var ou = this.extractTenant(window.location.host)?.okapiUrl;
        // console.log("call extract tenant(%s,%o) returns %s",window.location.host, this._tenant_cache, ou);
        this._url = ou;
      }
      return this._url;
    },
    
    grabDefaults: function () {
      const tenant_defaults = {
        "tenantIdentifier": this._tenant,
        "okapiUrl": this._url,
        "name":"knowint dev",
        "displayLabel":"K-int",
        "logoUrl":"https://www.k-int.com/wp-content/themes/k-int/images/k-int-logo.png"
      };
      
      return tenant_defaults;
	},
    
	extractTenant: function ( host ) {
      
      // Unify single declaration
		
      // console.log("extractTenant host is %s current tenant_cache is %o",host,tenant_cache);
      if (typeof window?.XMLHttpRequest === 'undefined') {
	    // Return the defaults but do not cache.
	    let defaults = this.grabDefaults();
	    console.log("XMLHttpRequest not available, use defaults %o", defaults);
	    return defaults;
	  }
      
      // Ensure a tenant cache
      if (typeof this._tenant_cache === 'undefined') {
		console.log("No tenant cache. Define first");
	    this._tenant_cache = {}
	  }
	  
	  let tenant_cache = this._tenant_cache;

      // For now, localhost:3000 is added to the list of resolvers
      // if ( ( host.startsWith('localhost')) || ( /^folio-ki-platform-.*-knowint.vercel.app$/.test(host) ) ) { 
      if ( /^folio-ki-platform-.*-knowint.vercel.app$/.test(host) ) {
		let defaults = this.grabDefaults();
        console.log("localhost or preview, use defaults %o", defaults);
        return defaults;
        // Use default above
      }
      
      // Valid for resolving, check cache first
      let result = tenant_cache[host];
      if ( typeof result != 'undefined' ) {
	    if (typeof result === 'function') {
		  result = result.call(this);
		}
	
        console.log("Cache hit on %s for get tenant %o",host,result);
        
        // Just return early.
        return result;
      }

	  // No cache hit, call resolver.
	  console.log("fetch tenant details for %s",host);
	  let oReq = new XMLHttpRequest();
          oReq.open("GET", "https://apps2.sph.k-int.com/tlpapi/public/resolve?hostname="+host, false);
	  oReq.send();
	  
	  // Set the result if we got a 200...
	  if (oReq.status === 200) {
            console.log("200 result %o",oReq.responseText);
	    result = JSON.parse(oReq.responseText)?.serviceProperties;
	    
            if (typeof result === 'undefined') {
	      // Success from the server, but no resolution for supplied host
	      // use the defaults.
	      console.error("Unable to resolve tenant details, due to unknown host. Using defaults and caching");
	      
	      // Add the actual function to the cache.
	      result = this.grabDefaults();
	      tenant_cache[host] = this.grabDefaults;
   	    } else {
              console.log("result undefined %s",oReq.responseText);
	      tenant_cache[host] = result;
	    }		
	  }
	  else {
	    console.error("Unable to resolve tenant details, due to server error. Using defaults");
	  }
	  
	  result 
	  

      console.log("extractTenant returns %o for %s",result,host);
      return result;
	},
	
	get tenant() {
	  const val = typeof this.tenantRuntime === 'function' ? this.tenantRuntime() : this._tenant;
	  console.log("Tenant getter called, returning %s",val);
          return val;
	},
	
    get url() {
	  const val = typeof this.urlRuntime === 'function' ? this.urlRuntime() : this._url;
	  console.log("URL getter called, returning %s",val);
      return val;
    },
    
    set url(val) {
	  console.log(`URL setter called with value ${val}`);
	  this._url = val;
	},
    
    set tenant(val) {
	  console.log(`Tenant setter called with value ${val}`);
	  this._tenant = val;
    }
  }
  
  return conf;
};

const generateTenantConfig = () => {
  const tenant_data = getTenantData();

  console.log("Got tenant data %o", tenant_data);
  
  return tenant_data;
}

const generateBranding = () => {
  console.error("Generate branding");
  return {
    logo: {
      src: './tenant-assets/k-int.png',
      src_nw: 'https://www.k-int.com/wp-content/themes/k-int/images/k-int-logo.png',
      alt: 'K-Int Sandbox 2',
    },
    favicon: {
      src: './tenant-assets/k-int-favicon.png',
    },
  }
}

module.exports = {
  okapi: generateTenantConfig(),
  config: {
    logCategories: 'core,path,action,xhr',
    logPrefix: '--',
    maxUnpagedResourceCount: 2000,
    showPerms: false
  },
  modules: {
    '@folio/acquisition-units': {},
    '@folio/agreements': {},
    '@folio/calendar' : {},
    '@folio/checkin' : {},
    '@folio/checkout' : {},
    '@folio/circulation' : {},
    '@folio/circulation-log' : {},
    '@folio/courses' : {},
    '@folio/dashboard': {},
    '@folio/data-export' : {},
    '@folio/data-import' : {},
    '@folio/developer' : {},
    '@folio/eholdings' : {},
    '@folio/erm-comparisons' : {},
    '@folio/erm-usage': {},
    '@folio/export-manager': {},
    '@folio/finance' : {},
    '@folio/handler-stripes-registry': {},
    '@folio/inventory' : {},
    '@folio/invoice': {},
    '@folio/ldp': {},
    '@folio/licenses': {},
    '@folio/local-kb-admin': {},
    '@folio/marc-authorities': {},
    '@folio/myprofile' : {},
    '@folio/notes' : {},
    '@folio/oai-pmh' : {},
    '@folio/orders': {},
    '@folio/organizations' : {},
    '@folio/plugin-bursar-export': {},
    '@folio/plugin-create-inventory-records' : {},
    '@folio/plugin-eusage-reports' : {},
    '@folio/plugin-find-agreement': {},
    '@folio/plugin-find-contact': {},
    '@folio/plugin-find-eresource': {},
    '@folio/plugin-find-erm-usage-data-provider': {},
    '@folio/plugin-find-fund': {},
    '@folio/plugin-find-import-profile' : {},
    '@folio/plugin-find-instance' : {},
    '@folio/plugin-find-interface' : {},
    '@folio/plugin-find-license': {},
    '@folio/plugin-find-organization': {},
    '@folio/plugin-find-package-title': {},
    '@folio/plugin-find-po-line': {},
    '@folio/plugin-find-user' : {},
    '@folio/plugin-find-organization': {},
    '@folio/quick-marc': {},
    '@folio/receiving' : {},
    '@folio/remote-storage' : {},
    '@folio/requests' : {},
    '@folio/servicepoints' : {},
    '@folio/service-interaction' : {},
    '@folio/stripes-erm-components': {},
    '@folio/tags': {},
    '@folio/tenant-settings' : {},
    '@folio/users' : {},
    '@folio/oa' : {},
    '@k-int/ill-directory': {},
    '@k-int/ill-request': {},
    '@k-int/ill-supply': {},
    '@k-int/ill-update': {},
    '@k-int/ill-ui': {}
  },
  removed: {
    '@folio/oa' : {},
    '@folio/remote-sync' : {},
  },
  branding: generateBranding()
};
